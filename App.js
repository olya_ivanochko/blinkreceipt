import React, { Component } from 'react';
import {
  FlatList,
  NativeModules,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';

const { BlinkReceipt } = NativeModules;

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      result: {}
    }
  }

  onPress = () => {
    BlinkReceipt.scan().then(result => {
      alert(JSON.stringify(result));
      this.setState({ result });
    }).catch(err => alert(err.message));
  }

  renderProductItem(item, index) {
    return (
      <View>
        <TouchableOpacity style={styles.itemContainer}>
          <Text>{item.productDescription.value}</Text>
          <Text>{item.totalPrice.toFixed(2)}</Text>
        </TouchableOpacity>
        <View
          style={{ height: 1, backgroundColor: '#eee' }}
        />
      </View>
    )
  }

  render() {
    const { result } = this.state;
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          {'Thank you for using\nreact-native-blink-receipt library!'}
        </Text>
        <TouchableOpacity onPress={this.onPress}>
          <Text style={styles.instructions}>
            To test, click here
          </Text>
        </TouchableOpacity>
        {!!result.total &&
          <Text style={styles.total}>
            {'Total: ' + result.total.value.toFixed(2)}
          </Text>
        }
        <FlatList
          keyExtractor={(item, index) => `list-item-${index}`}
          data={result.products ? result.products : []}
          renderItem={({ item, index }) => this.renderProductItem(item, index)}
          style={{ width: '100%', marginTop: 10 }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    marginVertical: 20
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  total: {
    width: '100%',
    textAlign: 'right',
    marginTop: 20,
    paddingHorizontal: 20
  },
  itemContainer: {
    flexDirection: 'row',
    height: 50,
    paddingHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'space-between',
  }
});
